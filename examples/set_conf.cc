/* set_conf.cc
 * Example jabberlue program
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contributor(s): Julian Missig
 */

#include <iostream>
#include <string>
#include <sigc++/object.h>
#include <gtk--/main.h>
#include <jabberlue.hh>
#include <jabberlue-conf-write.hh>
#include <jabberlue-conf-read.hh>

class ConfTest : public SigC::Object
{
public:
     ConfTest(int argc, char* argv[]);
     ~ConfTest();
private:
     JabberlueConfWrite* _JConf;
     JabberlueConfRead* _RJConf;
};

ConfTest::ConfTest(int argc, char* argv[])
     : _JConf(new JabberlueConfWrite(argc, argv)),
       _RJConf(new JabberlueConfRead(argc, argv))
{
     cout << "ConfTest" << endl;
     cout << "--------" << endl;
     cout << "username: ";
     cout << _RJConf->get_username() << endl;
     cout << "server: ";
     cout << _RJConf->get_server() << endl;
     cout << "password: ";
     cout << _RJConf->get_password() << endl;
     cout << "--------" << endl;
     cout << "username: ";
     string username;
     cin  >> username;
     cout << "server: ";
     string server;
     cin  >> server;
     cout << "password: ";
     string password;
     cin  >> password;

     cout << "syncing... ";
     _JConf->set_username(username);
     _JConf->set_server(server);
     _JConf->set_port(5222);
     _JConf->set_password(password);
     _JConf->sync();
     cout << "done." << endl;

     Gtk::Main::quit();
}

ConfTest::~ConfTest()
{
     delete _JConf;
}

int main (int argc, char* argv[])
{
     Gtk::Main kit(argc, argv);
 
     ConfTest conftest(argc, argv);

     kit.run();
     return 0;
}
