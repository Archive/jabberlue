/* main.cc
 * Example jabberlue program
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contributor(s): Julian Missig
 */

#include <iostream>
#include <string>
#include <sigc++/object.h>
#include <gtk--/main.h>
#include <jabberlue.hh>

static Jabberlue::Session* jabber = NULL;

class JabberTest : public SigC::Object
{
public:
     JabberTest();
     ~JabberTest();
protected:
     gint on_timeout();
     void on_connected();
     void on_disconnected();
     void on_duplicate();
     void on_msg(const Jabberlue::Message& m);
     void on_pres(const Jabberlue::Presence& p, bool changed);
private:
     SigC::Connection _timer;
};

JabberTest::JabberTest()
{
     string password, resource;
     cout << "Password: ";
     cin  >> password;
     cout << "Resource: ";
     cin  >> resource;

     jabber->setResource(resource);
     jabber->setPassword(password);
     jabber->connect();
     jabber->evtConnected.connect(slot(this, &JabberTest::on_connected));
     jabber->evtDisconnected.connect(slot(this, &JabberTest::on_disconnected));
     jabber->errResourceDuplicate.connect(slot(this, &JabberTest::on_duplicate));
     jabber->evtMessage.connect(slot(this, &JabberTest::on_msg));
     jabber->evtPresence.connect(slot(this, &JabberTest::on_pres));
     _timer = Gtk::Main::timeout.connect(slot(this, &JabberTest::on_timeout), 5000);
}

JabberTest::~JabberTest()
{
     cerr << "~JabberTest()" << endl;
     delete jabber;
}

gint JabberTest::on_timeout()
{
     _timer.disconnect();
     string should_continue;
     cout << "Continue? (Y/n): ";
     cin  >> should_continue;
     if (should_continue == "n")
     {
	  jabber->disconnect();
	  Gtk::Main::quit();
	  return TRUE;
     }
     _timer = Gtk::Main::timeout.connect(slot(this, &JabberTest::on_timeout), 5000);
     return TRUE;
}

void JabberTest::on_connected()
{
     cout << "Connected!" << endl;
     jabber->setPresence(jabberoo::Presence::stOnline);
}

void JabberTest::on_disconnected()
{
     cout << "Disconnected!" << endl;
}

void JabberTest::on_duplicate()
{
     cout << "Resource already exists!" << endl;
     jabber->disconnect();
     Gtk::Main::quit();
}

void JabberTest::on_msg(const Jabberlue::Message& m)
{
     cout << "Message received from: " << m.getFrom() << endl;
     m.is_displayed(*jabber);
     cout << m.getBody() << endl;
}

void JabberTest::on_pres(const Jabberlue::Presence& p, bool changed)
{
     cout << "Presence received from: " << p.getFrom() << endl;
}

int main (int argc, char* argv[])
{
     Gtk::Main kit(argc, argv);

     gchar* temp = "";

     jabber = new Jabberlue::Session(0, &temp);

     JabberTest jabtest;

     kit.run();
     return 0;
}
