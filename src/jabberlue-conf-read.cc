/* jabberlue-conf.cc
 * Centralized configuration of Jabber Stuff
 *
 * Copyright (c) 2001-2002 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contributor(s): Julian Missig
 */

#include "jabberlue-conf-read.hh"
#include <iostream>
#include <string>
#include <sigc++/slot.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>


using SigC::slot;
using namespace std;

// -------------------------------------------------------------------------
// JabberlueConfRead
// -------------------------------------------------------------------------
JabberlueConfRead::JabberlueConfRead(int argc, gchar *argv[])
{
     // Start up GConf
     if (!gconf_is_initialized())
	  gconf_init(argc, argv, 0);

     // Get the client
     _gc = gconf_client_get_default();
     // Place to store it all
     gconf_client_add_dir(_gc, JABBERCONFDIR.c_str(), 
			  GCONF_CLIENT_PRELOAD_ONELEVEL, 0);

     // Connection
     // Add the directory
     gconf_client_add_dir(_gc, CONNECTIONDIR.c_str(),
			  GCONF_CLIENT_PRELOAD_ONELEVEL, 0);
     // Hook up notification
     //_gc->notify_add(CONNECTIONDIR, slot(this, &JabberlueConfRead::notify_connection));

     // User Info
     // Add the directory
     gconf_client_add_dir(_gc, USERINFODIR.c_str(),
			  GCONF_CLIENT_PRELOAD_ONELEVEL, 0);
     // Hook up notification
     //_gc->notify_add(USERINFODIR, slot(this, &JabberlueConfRead::notify_userinfo));
}

JabberlueConfRead::~JabberlueConfRead()
{
     // Free some memory
     if (_gc)
	  gconf_client_clear_cache(_gc);
}

void JabberlueConfRead::sync()
{
     // sync to hard drive
     gconf_client_suggest_sync(_gc, 0);
}

string JabberlueConfRead::getString(gchar* gtemp)
{
     if (gtemp != NULL)
     {
	  string stemp = gtemp;
	  g_free(gtemp);
	  return stemp;
     }
     g_free(gtemp);
     return string("");
}

// -------------------------------------------------------------------------
// Connection
// -------------------------------------------------------------------------
string JabberlueConfRead::get_username()
{
     return getString(gconf_client_get_string(_gc, string(CONNECTIONDIR + "/username").c_str(), 0));
}

bool JabberlueConfRead::get_save_password()
{
     return gconf_client_get_bool(_gc, string(CONNECTIONDIR + "/save_password").c_str(), 0);
}

string JabberlueConfRead::get_password()
{
     return getString(gconf_client_get_string(_gc, string(CONNECTIONDIR + "/password").c_str(), 0));
}

string JabberlueConfRead::get_server()
{
     return getString(gconf_client_get_string(_gc, string(CONNECTIONDIR + "/server").c_str(), 0));
}

gint JabberlueConfRead::get_port()
{
     return gconf_client_get_int(_gc, string(CONNECTIONDIR + "/port").c_str(), 0);
}

bool JabberlueConfRead::get_ssl()
{
     return gconf_client_get_bool(_gc, string(CONNECTIONDIR + "/ssl").c_str(), 0);
}

// -------------------------------------------------------------------------
// User Info
// -------------------------------------------------------------------------
string JabberlueConfRead::get_given_name()
{
     return getString(gconf_client_get_string(_gc, string(USERINFODIR + "/given_name").c_str(), 0));
}

string JabberlueConfRead::get_family_name()
{
     return getString(gconf_client_get_string(_gc, string(USERINFODIR + "/family_name").c_str(), 0));
}

string JabberlueConfRead::get_full_name()
{
     return getString(gconf_client_get_string(_gc, string(USERINFODIR + "/full_name").c_str(), 0));
}

string JabberlueConfRead::get_nickname()
{
     return getString(gconf_client_get_string(_gc, string(USERINFODIR + "/nickname").c_str(), 0));
}

string JabberlueConfRead::get_email()
{
     return getString(gconf_client_get_string(_gc, string(USERINFODIR + "/email").c_str(), 0));
}

string JabberlueConfRead::get_country()
{
     return getString(gconf_client_get_string(_gc, string(USERINFODIR + "/country").c_str(), 0));
}

