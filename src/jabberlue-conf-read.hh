/* jabberlue-conf-read.hh
 * Centralized configuration of Jabber Stuff
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contributor(s): Julian Missig
 */

#ifndef INCL_JABBERLUE_CONF_READ_HH
#define INCL_JABBERLUE_CONF_READ_HH

#include <sigc++/object.h>
#include <sigc++/signal_system.h>
#include <gconf/gconf-client.h>
#include <string>

// All of the paths
const std::string JABBERCONFDIR = "/apps/jabber";
const std::string CONNECTIONDIR = "/apps/jabber/connection";
const std::string USERINFODIR   = "/apps/jabber/userinfo";
const std::string GPGDIR        = "/apps/jabber/gpg";

class JabberlueConfRead
     : public SigC::Object
{
public:
     JabberlueConfRead(int argc, gchar *argv[]);
     ~JabberlueConfRead();
     void sync();
protected:
     std::string getString(gchar* gtemp);
public:
     // GConfClient
     GConfClient* getGConfClient() { return _gc; }
     // Connection
     std::string get_username();
     bool   get_save_password();
     std::string get_password();
     std::string get_server();
     gint   get_port();
     bool   get_ssl();
     
     // User Info
     std::string get_given_name();
     std::string get_family_name();
     std::string get_full_name();
     std::string get_nickname();
     std::string get_email();
     std::string get_country();

private:
     GConfClient*     _gc;

public:
     SigC::Signal0<void> connection_conf_changed;
     SigC::Signal0<void> userinfo_conf_changed;
};

#endif // INCL_JABBERLUE_CONF_READ_HH
