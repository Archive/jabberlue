/* jabberlue.cc
 * Jabber protocol stack
 *
 * Copyright (c) 2001-2002 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact:
 *   
 *   IBM Advanced Internet Technology Group
 *   c/o Lotus Development Corporation
 *   One Rogers Street
 *   Cambridge, MA 02142
 *   USA
 *
 * Contributor(s): Julian Missig
 */

#include "jabberlue.hh"

#include <string>
#include <iostream>
#include <sigc++/signal_system.h>

// For uname
#include <sys/utsname.h>

#include "jabberlue-conf-read.hh"
#include "jabberoo.hh"

using SigC::slot;
using namespace Jabberlue;


// -------------------------------------------------------------------------
// Jabberlue Session
// -------------------------------------------------------------------------
Session::Session(int argc, gchar *argv[])
     : _JConf(new JabberlueConfRead(argc, argv)),
       _Session(new jabberoo::Session),
       _Transmitter(new TCPTransmitter),
       _autoreconnect(false),
       _new_user(false)
{
#ifdef DEBUG
     cerr << "Jabberlue: Session()" << endl;
#endif
     // Connect session debugging signals
     _Session->evtTransmitXML.connect(slot(this, &Session::transmit_XML));
     _Session->evtRecvXML.connect(slot(this, &Session::recv_XML));

     // Connect session signals
     _Session->evtTransmitXML.connect(slot(_Transmitter, &TCPTransmitter::send));
     _Session->evtConnected.connect(slot(this, &Session::on_session_connected));
     _Session->evtDisconnected.connect(slot(this, &Session::on_session_disconnected));

     // Connect transmitter signals
     _Transmitter->evtConnected.connect(slot(this, &Session::on_transmitter_connected));
     _Transmitter->evtDisconnected.connect(slot(this, &Session::on_transmitter_disconnected));
     _Transmitter->evtError.connect(slot(this, &Session::on_transmitter_error));
     _Transmitter->evtDataAvailable.connect(slot(_Session, &jabberoo::Session::push));

     // Connect more session signals
     _Session->evtMessage.connect(slot(this, &Session::on_session_message));
     _Session->evtIQ.connect(slot(this, &Session::on_session_iq));
     _Session->evtPresence.connect(slot(this, &Session::on_session_presence));
     _Session->evtPresenceRequest.connect(slot(this, &Session::on_session_presence_request));
//     _Session->evtUnknownPacket.connect(slot(this, &Session::on_session_unknown));
     _Session->evtAuthError.connect(slot(this, &Session::on_session_auth_error));
     _Session->evtOnVersion.connect(slot(this, &Session::on_session_version));
//     _Session->evtOnLast.connect(slot(this, &Session::on_session_last));

	  _password = _JConf->get_password();
}

Session::~Session()
{
#ifdef DEBUG
     cerr << "Session: ~Session()" << endl;
#endif
     delete _JConf;
     delete _Session;
     delete _Transmitter;
}

void Session::connect()
{
#ifdef DEBUG
     cerr << "Jabberlue: connect()" << endl;
#endif
     // Check for settings
     if (_JConf->get_server().empty() || _JConf->get_username().empty())
     {
#ifdef DEBUG
	  cerr << "Jabberlue: error: No config present." << endl;
#endif
	  errNoConfig(); // It appears no config has been set...
	  return;
     }

     // Check for the password
     if (_password.empty())
     {
	  _password = _JConf->get_password();
     }
     if (_password.empty())
     {
#ifdef DEBUG
	  cerr << "Jabberlue: error: No password present." << endl;
#endif
	  errNoPassword(); // They didn't give us a password and it wasn't saved in JabberlueConf either
	  return;
     }

     // Check the resource
     if (_resource.empty())
     {
#ifdef DEBUG
	  cerr << "Jabberlue: error: No resource present." << endl;
#endif
	  errNoResource();
	  return;
     }

     // Resource checking
     GSList* list_resources = gconf_client_get_list(_JConf->getGConfClient(), string(CONNECTIONDIR + "/resources").c_str(), GCONF_VALUE_STRING, 0);
     gchar* gresource = g_strdup(_resource.c_str());

/*
     if (g_slist_find_custom(list_resources, gresource, (GCompareFunc)g_strcasecmp) != NULL)
     {
  // This resource already exists!
	  errResourceDuplicate();
#ifdef DEBUG
	  cerr << "Jabberlue: Resource already exists in list!" << endl;
#endif
	  g_slist_free(list_resources);
	  g_free(gresource);
	  return;
     }
*/

     list_resources = g_slist_prepend(list_resources, gresource);
     if (!gconf_client_set_list(_JConf->getGConfClient(), string(CONNECTIONDIR + "/resources").c_str(), GCONF_VALUE_STRING, list_resources, 0))
     {
	  cerr << "Jabberlue: GConf list set failed. Oh well." << endl;
     }
     _JConf->sync();

     g_slist_free(list_resources);
     g_free(gresource);
#ifdef DEBUG
     cerr << "Jabberlue: Transmitter connect: " << _JConf->get_server() << ":" << _JConf->get_port() << endl;
#endif
     // Attempt connecting to a server
     _Transmitter->connect(_JConf->get_server(),
			   _JConf->get_port(),
			   _JConf->get_ssl(),
			   _autoreconnect);
}

void Session::disconnect()
{
     _Session->disconnect();
}

const string Session::getUsername() const
{
     if (_Session->getConnState() != jabberoo::Session::csNotConnected)
	  return _username;
     else
	  return _JConf->get_username();
}

const string Session::getRemoteHost() const
{
     if (_Session->getConnState() != jabberoo::Session::csNotConnected)
	  return _server;
     else
	  return _JConf->get_server();
}

const int Session::getRemotePort() const
{
     if (_Session->getConnState() != jabberoo::Session::csNotConnected)
	  return _port;
     else
	  return _JConf->get_port();
}

void Session::setResource(const string& resource)
{
     // Check the resource
     if (resource.empty())
     {
	  errNoResource();
	  return;
     }
     _resource = resource;
}

const string Session::getResource() const
{
     return _resource;
}

void Session::setPassword(const string& password)
{
     _password = password;
}

const string Session::getPassword() const
{
     return _password;
}

const string Session::getJID() const
{
     if (_Session->getConnState() != jabberoo::Session::csNotConnected)
	  return _username + "@" + _server + "/" + _resource;
     else
	  return _JConf->get_username() + "@" + _JConf->get_server() + "/" + _resource;
}

void Session::setAutoreconnect(bool autoreconnect)
{
     _autoreconnect = autoreconnect;
}

void Session::setNewUser(bool new_user)
{
     _new_user = new_user;
}

void Session::removeResource(const string& resource)
{
     // Remove a resource from the list
     GSList* list_resources = gconf_client_get_list(_JConf->getGConfClient(), string(CONNECTIONDIR + "/resources").c_str(), GCONF_VALUE_STRING, 0);
     gchar* gresource = g_strdup(resource.c_str());
     GSList* list_item = g_slist_find_custom(list_resources, gresource, (GCompareFunc)g_strcasecmp);
     list_resources = g_slist_remove_link(list_resources, list_item);
     g_slist_free(list_item);

     if (!gconf_client_set_list(_JConf->getGConfClient(), string(CONNECTIONDIR + "/resources").c_str(), GCONF_VALUE_STRING, list_resources, 0))
     {
#ifdef DEBUG
	  cerr << "Jabberlue: GConf list set failed!" << endl;
#endif
     }
     _JConf->sync();
     g_slist_free(list_resources);
     g_free(gresource);
}

void Session::send(const jabberoo::Packet& p)
{
     *_Session << p;
}

void Session::send(const string& xml)
{
     *_Session << xml;
}

void Session::setPresence(jabberoo::Presence::Show stype, const string& status, const string& priority)
{
     // Send the new presence
     send(Jabberlue::Presence("", jabberoo::Presence::ptAvailable, stype, status, priority));
}

jabberoo::Session& Session::getSession()
{
     return *_Session;
}

void Session::transmit_XML(const char* XML)
{
//     cerr << jutil::getTimeStamp() 
#ifdef DEBUG
     cerr << "Jabberlue: send " << XML << endl;
#endif
}

void Session::recv_XML(const char* XML)
{
//     cerr << jutil::getTimeStamp() 
#ifdef DEBUG
     cerr << "Jabberlue: recv " << XML << endl;
#endif
}

void Session::on_session_connected(const judo::Element& tag)
{
     // This is our initial connection
     _initial_connection = _Session->evtOnRoster.connect(slot(this, &Session::on_roster));
}

void Session::on_roster()
{
     // We fire evtConnected here because we only want
     // presence operations after jabberoo is ready for them
     evtConnected();
     _initial_connection.disconnect();
}

void Session::on_session_disconnected()
{
     // Fire event
     evtDisconnected();

     // Remove the resource
     removeResource(_resource);

     // Disconnect the transmitter
     _Transmitter->disconnect();
}

void Session::on_transmitter_connected()
{
     // Begin auth...
     jabberoo::Session::AuthType atype = jabberoo::Session::atAutoAuth;

     // Save all the remote config...
     _username = _JConf->get_username();
     _server = _JConf->get_server();
     _port = _JConf->get_port();

#ifdef DEBUG
     cerr << "Jabberlue: Session connect: " << _server << ", " << atype << ", " << _username << ", " << _resource << ", " << _password << endl;
#endif
     _Session->connect(_server,
		       atype,
		       _username,
		       _resource,
		       _password,
		       _new_user);
}

void Session::on_transmitter_disconnected()
{
     if (_Session)
     {
          // Tell the session to disconnect
#ifdef DEBUG
          cerr << "DISCONNECTED" << endl;
#endif
          _Session->disconnect();
     }
}

void Session::on_transmitter_error(const string & emsg)
{
#ifdef DEBUG
     cerr << "Transmitter error. Disconnected: " << emsg << endl;
#endif
     _Transmitter->disconnect();
}

void Session::on_session_message(const jabberoo::Message& m)
{
     if (m.getType() == jabberoo::Message::mtError)
     {
	  errMessage(Jabberlue::Message(m));
	  return;
     }
     else if (m.findX("jabber:x:event"))
     {
	  Element* x = m.findX("jabber:x:event");
	  if (x)
	  {
	       if (m.getBody().empty())
	       {
		    // Since the body is empty, this is an event in response
		    // to a previously sent message
		    if (x->findElement("delivered"))
			 msgDelivered(m.getID(), m.getThread(), m.getFrom());
		    if (x->findElement("displayed"))
			 msgDisplayed(m.getID(), m.getThread(), m.getFrom());
		    if (x->findElement("composing"))
			 msgComposing(m.getID(), m.getThread(), m.getFrom());
		    return;
	       }
	       else
	       {
		    // The body was not empty, so they are requesting events
		    // This message has been delivered to the client
		    // so we send a delivered event
		    if (x->findElement("delivered"))
			 send(m.delivered());
	       }
	  }
     }

     // Unless returned above, we send out message event
     evtMessage(Jabberlue::Message(m));
}

void Session::on_session_iq(const judo::Element& iq)
{
     evtIQ(jabberoo::Packet(iq));
}

void Session::on_session_presence(const jabberoo::Presence& p, const jabberoo::Presence::Type prev_type)
{
     // Set whether Presence::Type changed
     evtPresence(Jabberlue::Presence(p), !(p.getType() == prev_type));
}

void Session::on_session_presence_request(const jabberoo::Presence& p)
{
     evtPresenceRequest(Jabberlue::Presence(p));
}

void Session::on_session_auth_error(int ErrorCode, const char* ErrorMsg)
{
     evtAuthError(ErrorCode, ErrorMsg);
}

void Session::on_session_version(string& name, string& version, string& os)
{
     // Run uname
     struct utsname osinfo;
     uname(&osinfo);
     os = string(osinfo.sysname) + " " + string(osinfo.release) + " " + string(osinfo.machine);

     // Request client name and client version
     evtOnVersion(name, version);
}


// -------------------------------------------------------------------------
// Jabberlue Presence
// -------------------------------------------------------------------------
Presence::Presence(const string& jid, jabberoo::Presence::Type ptype, jabberoo::Presence::Show stype, const string& status, const string& priority)
     : jabberoo::Presence(jid, ptype, stype, status, priority)
{
}

Presence::Presence(const jabberoo::Presence& p)
     : jabberoo::Presence(p.getBaseElement())
{
}

Presence::~Presence()
{
}

void Presence::AddExtension(const string& xmlns, const judo::Element& t)
{
     Element* x = addX(xmlns);
//     x->addElement(t);
}


// -------------------------------------------------------------------------
// Jabberlue Message
// -------------------------------------------------------------------------
Message::Message(const string& jid, const string& body, jabberoo::Message::Type mtype)
     : jabberoo::Message(jid, body, mtype)
{
}

Message::Message(const jabberoo::Message& m)
     : jabberoo::Message(m.getBaseElement())
{
}

Message::~Message()
{
}

void Message::is_displayed(Jabberlue::Session& s) const
{
     Element* x = findX("jabber:x:event");
     if (x && x->findElement("displayed"))
     {
	  if (!getBody().empty())
	  {
	       // The body was not empty, so they are requesting events
	       // Send the displayed message
	       s.getSession() << displayed();
	  }
     }
}

void Message::is_composing(Jabberlue::Session& s) const
{
     Element* x = findX("jabber:x:event");
     if (x && x->findElement("composing"))
     {
	  if (!getBody().empty())
	  {
	       // The body was not empty, so they are requesting events
	       // Send the composing message
	       s.getSession() << composing();
	  }
     }
}

void Message::AddExtension(const string& xmlns, const judo::Element& t)
{
     Element* x = addX(xmlns);
//     x->addElement(t);
}


// -------------------------------------------------------------------------
// Jabberlue IQ
// -------------------------------------------------------------------------
IQ::IQ(jabberoo::Session& s, const string& jid)
     : jabberoo::Packet("iq"),
       _Session(s),
       _id(_Session.getNextID())
{
     if (!jid.empty())
	  getBaseElement().putAttrib("to", jid);
     getBaseElement().putAttrib("id", _id);
}

IQ::~IQ()
{
}

void IQ::setCallback(jabberoo::ElementCallbackFunc f)
{
     _Session.registerIQ(_id, f);
}

judo::Element& IQ::addQuery(const string& xmlns)
{
     judo::Element* query = getBaseElement().addElement("query");
     query->putAttrib("xmlns", xmlns);

     return *query;
}


// -------------------------------------------------------------------------
// Jabberlue PresenceInfo
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// Jabberlue RosterItem
// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// Jabberlue Roster
// -------------------------------------------------------------------------
