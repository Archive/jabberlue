/* jabberlue.hh
 * Jabber protocol stack
 *
 * Copyright (c) 2001-2002 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contributor(s): Julian Missig
 */

#ifndef INCL_JABBERLUE_HH
#define INCL_JABBERLUE_HH

#include "jabberlue-conf-read.hh"
#include <jabberoo.hh>
#include "TCPTransmitter.hh"
#include <sigc++/signal_system.h>

/** 
 * Base Jabberlue namespace.
 * This namespace contains nice interfaces to Jabber stuff.
 */
namespace Jabberlue
{
     class Presence;
     class Message;
     class IQ;
     class Session;

     /** 
      * Presence Packet.
      * With this class you can get/set all the basic presence options.
      */
     class Presence : public jabberoo::Presence
     {
     public:

	  /**
	   * Construct a Presence Packet based upon given values.
	   * It probably makes more sense to use Jabberlue::createPresence()
	   * @see Jabberlue::createPresence()
	   * @see jabberoo::Presence
	   * @see jabberoo::Presence::Type
	   * @see jabberoo::Presence::Show
	   * @param jid The JabberID to send this presence to. An empty string sends to everyone who has the proper subscription.
	   * @param ptype The jabberoo::Presence::Type of presence to send.
	   * @param stype The jabberoo::Presence::Show for the presence, jabberoo::Presence::stInvalid leaves it blank.
	   * @param status The status message for the presence. Can be an empty string.
	   * @param priority The priority of this presence. Should be a string of an int which is 0 or greater. Empty string sets it to 0.
	   */
	  Presence(const string& jid, jabberoo::Presence::Type ptype, jabberoo::Presence::Show stype = jabberoo::Presence::stInvalid, const string& status = "", const string& priority = "0");

	  /**
	   * Construct a Jabberlue Presence Packet based upon a jabberoo::Presence.
	   * It probably makes more sense to use Jabberlue::createPresence()
	   * @see Jabberlue::createPresence()
	   * @see jabberoo::Presence
	   * @param p The jabberoo::Presence to use.
	   */
	  Presence(const jabberoo::Presence& p);

	  /**
	   * Presence destructor.
	   * Doesn't really do much worth noting.
	   * @see Presence()
	   */
	  ~Presence();

	  /**
	   * Add an XML extension to the Presence Packet.
	   * @see judo::Element()
	   */
	  void AddExtension(const string& xmlns, const judo::Element& t);
     };

     /**
      * Message Packet.
      */
     class Message : public jabberoo::Message
     {
     public:
	  /**
	   * Construct a Message Packet based upon given values.
	   * It probably makes more sense to use Jabberlue::createMessage()
	   * @see Jabberlue::createMessage()
	   * @see jabberoo::Message
	   * @see jabberoo::Message::Type
	   * @param jid The JabberID to send to.
	   * @param body The body of the message. Can be an empty string.
	   * @param mtype The message type, defaults to normal.
	   */
	  Message(const string& jid, const string& body, jabberoo::Message::Type mtype = jabberoo::Message::mtNormal);
	  
	  /**
	   * Construct a Jabberlue Message Packet based upon a jabberoo::Message.
	   * It probably makes more sense to use Jabberlue::createMessage()
	   * @see Jabberlue::createMessage()
	   * @see jabberoo::Message
	   * @param m The jabberoo::Message to use.
	   */
	  Message(const jabberoo::Message& m);

	  /**
	   * Destruct a Message Packet.
	   * Doesn't really do much worth noting.
	   * @see Message()
	   */
	  ~Message();

	  /**
	   * Indicate that this Message has been displayed.
	   * Sends a displayed message event to sender of this Message if they requested one.
	   * @param s The Jabberlue::Session to use.
	   */
	  void is_displayed(Jabberlue::Session& s) const;

	  /**
	   * Indicate that a reply to this Message is being composed.
	   * Sends a composing message event to sender of this Message if they requested one. 
  	   * @param s The Jabberlue::Session to use.
	   */
	  void is_composing(Jabberlue::Session& s) const;

	  /**
	   * Add an XML extension to the Message Packet.
	   * @see judo::Element()
	   */
	  void AddExtension(const string& xmlns, const judo::Element& t);
     };
   
     /**
      * IQ Packet.
      */
     class IQ : public jabberoo::Packet
     {
     public:
	  /**
	   * Construct an Information Query Packet.
	   * Within Jabber, IQ packets are used for various kinds of information
	   * retrieval which does not make sense to be within a Message.
	   * It probably makes more sense to use Jabberlue::createIQ()
	   * @see jabberoo::Packet
	   * @see judo::Element
	   * @param s The jabberoo::Session to use.
	   * @param jid The JabberID to send the IQ Packet to. Can be an empty string, which would send it to the server.
	   */
	  IQ(jabberoo::Session& s, const string& jid);
	  
	  /**
	   * Destruct an Information Query Packet.
	   * Doesn't really do much worth noting.
	   * @see IQ()
	   */
	  ~IQ();

	  /**
	   * Register a callback function.
	   * After the IQ is sent, Jabberlue will wait for a response to this specific IQ
	   * and then call the given function.
	   * @see jabberoo::ElementCallbackFunc
	   * @see jabberoo::Session::registerIQ()
	   * @see judo::Element
	   * @param f The function to be called. It should return void and take a judo::Element& parameter.
	   */
	  void setCallback(jabberoo::ElementCallbackFunc f);

	  /**
	   * Add a query element to the IQ Packet.
	   * @see judo::Element
	   * @param xmlns The XML namespace of the query element to add.
	   * @return The query element which was added.
	   */ 
	  judo::Element& Jabberlue::IQ::addQuery(const string& xmlns);

     private:
	  jabberoo::Session& _Session;
	  string             _id;
     };

     /**
      * Jabber Session.
      */
     class Session : public SigC::Object 
     {
     public:
	  /** 
	   * Session constructor.
	   * This is intended to be used in main() and should thus take main()'s 
	   * arguments so they may be passed on to the GConf initilization.
	   * @param argc
	   * @param argv
	   */
	  Session(int argc, gchar *argv[]);
	  
	  /**
	   * Session destructor.
	   * Ends any open Jabber sessions and cleans up.
	   * @see Session();
	   */
	  ~Session();
	  
	  /** 
	   * Connect to jabber.
	   * Connect to jabber with the resource passed as an argument. The 
	   * resource is a unique Jabber session identifier. Jabberlue 
	   * gathers all other connection configuration from GConf.
	   * This function may fire: errNoConfig, errNoPassword, 
	   * errNoResource, errResourceDuplicate.
	   * When complete, this function fires: evtConnected.
	   * @see errNoConfig
	   * @see errNoPassword
	   * @see errNoResource
	   * @see errResourceDuplicate
	   * @see evtConnected
	   */
	  void connect();
	  
	  /** 
	   * Disconnect from jabber.
	   * Disconnects and frees the resource for use by another instance
	   * of the library.
	   */
	  void disconnect();
	  
	  /**
	   * Get the username.
	   * @return The username if you really need it.
	   */
	  const string getUsername() const;
	  
	  /**
	   * Get the remote host (server).
	   * @return The remote host if you really need it.
	   */
	  const string getRemoteHost() const;
	  
	  /**
	   * Get the remote port.
	   * @return The remote port if you really need it.
	   */
	  const int getRemotePort() const;
	  
	  /**
	   * Set the resource.
	   * Set the resource for this session. The resource must be unique
	   * to the user since this identifies this connection.
	   * @see errNoResource
	   * @see errResourceDuplicate
	   * @param resource a unique Jabber session identifier string
	   */
	  void setResource(const string& resource);
	  
	  /**
	   * Get the resource.
	   * @return The resource if you really need it.
	   */
	  const string getResource() const;
	  
	  /**
	   * Set the password.
	   * Set the password which should be used to connect to Jabber.
	   * If the user decided to allow the password to be saved, this
	   * is not needed.
	   * @see errNoPassword
	   * @param password the string password
	   */
	  void setPassword(const string& password);
	  
	  /**
	   * Get the password.
	   * @return The password if you really need it.
	   */
	  const string getPassword() const;
	  
	  /**
	   * Get the JabberID.
	   * @return The likely JabberID the user is connected or will connect with.
	   */
	  const string getJID() const;
	  
	  /**
	   * Set whether to reconnect
	   * If enabled, Jabberlue will attempt to reconnect when the 
	   * session is unexpectedly dropped.
	   * @param autoreconnect whether or not Jabberlue should attempt to reconnect
	   */
	  void setAutoreconnect(bool autoreconnect);
	  
	  void setNewUser(bool new_user);
	  
	  /**
	   * Remove a resource from the internal list.
	   * This should not normally be used, but in some cases it may be needed
	   * to remove a resource from the list of connected resources, since 
	   * Jabberlue will not allow multiple connections with the same resource.
	   * This will not disconnect the given resource from Jabber.
	   * Again, you do not normally need to call this.
	   * @see connect()
	   * @see errResourceDuplicate
	   * @param resource a unique Jabber session identifier string
	   */
	  void removeResource(const string& resource);
	  
	  /**
	   * Send a jabberoo::Packet.
	   * This function sends a jabberoo::Packet to the Jabber server.
	   * @see jabberoo::Packet
	   * @param p The jabberoo::Packet to send.
	   */
	  void send(const jabberoo::Packet& p);
	  
	  /**
	   * Send raw XML.
	   * This function sends raw XML to the Jabber server. Be careful!
	   * You can get disconnected for malformed XML.
	   * @param xml The XML to send.
	   */
	  void send(const string& xml);
	  
	  /**
	   * Set the presence.
	   * @param stype The jabberoo::Presence::Show for the presence.
	   * @param status The status message for the presence. Can be an empty string.
	   * @param priority The priority of this presence. Should be a string of an int which is 0 or greater. Empty string sets it to 0.
	   */
	  void setPresence(jabberoo::Presence::Show stype, const string& status = "", const string& priority = "0");
	  
	  /**
	   * Direct access to the jabberoo::Session.
	   * In most cases, this function should not be needed. But
	   * if you wish to do something to the session which Jabberlue does
	   * not wrap, it's here. Avoid using if possible.
	   * @see jabberoo::Session
	   * @return A reference to the internally connected jabberoo::Session
	   */
	  jabberoo::Session& getSession();
	  
	  /**
	   * Send a packet.
	   * @see Jabberlue::send()
	   */
	  jabberoo::Session& operator<<(const jabberoo::Packet& p) { send(p); return *_Session; }     
     protected:
	  void transmit_XML(const char* XML);
	  void recv_XML(const char* XML);
	  void on_session_connected(const judo::Element& tag);
	  void on_roster();
	  void on_session_disconnected();
	  void on_transmitter_connected();
	  void on_transmitter_disconnected();
	  void on_transmitter_error(const string & emsg);
	  void on_session_message(const jabberoo::Message& m);
	  void on_session_iq(const judo::Element& iq);
	  void on_session_presence(const jabberoo::Presence& p, const jabberoo::Presence::Type prev_type);
	  void on_session_presence_request(const jabberoo::Presence& p);
	  void on_session_auth_error(int ErrorCode, const char* ErrorMsg);
	  void on_session_version(string& name, string& version, string& os);
	  
     private:
	  JabberlueConfRead* _JConf;
	  jabberoo::Session* _Session;
	  TCPTransmitter*    _Transmitter;
	  string             _username;
	  string             _server;
	  int                _port;
	  string             _resource;
	  string             _password;
	  bool               _autoreconnect;
	  bool               _new_user;
	  SigC::Connection   _initial_connection;
	  
     public:
	  /** 
	   * No configuration set event.
	   * This event is fired when the user has no used an external utility
	   * to set up Jabber configuration ahead of time.
	   * Use the separate JabberlueConf library to set the needed values.
	   * @see connect()
	   */
	  SigC::Signal0<void>      errNoConfig;
	  
	  /** 
	   * No password set event.
	   * This event is fired when the password was not set. Request the
	   * user's password and then call the setPassword function.
	   * @see setPassword()
	   * @see connect()
	   */
	  SigC::Signal0<void>      errNoPassword;
	  
	  /** 
	   * No resource set event.
	   * This event is fired when the resource was set to NULL. This is 
	   * not allowed.
	   * @see connect()
	   */
	  SigC::Signal0<void>      errNoResource;
	  
	  /** 
	   * Resource duplicate event.
	   * This event is fired when it appears that the user alread has
	   * another Jabber session connected with the given resource.
	   * Ask for a different resource and attempt to connect() again.
	   * @see connect()
	   */
	  SigC::Signal0<void>      errResourceDuplicate;
	  
	  /** 
	   * Connected event.
	   * This is fired when Jabberlue has successfully connected to 
	   * a Jabber server and is ready to begin sending Jabber packets.
	   * @see connect()
	   */
	  SigC::Signal0<void>      evtConnected; 
	  
	  /**
	   * Disconnected event.
	   * This event is fired when Jabberlue was disconnected, either
	   * on purpose or due to error. The resource used to connect
	   * should be available for another connection.
	   * @see disconnect()
	   */
	  SigC::Signal0<void>      evtDisconnected;

	  /**
	   * Message error event.
	   * This event is fired when an error regarding messaging is
	   * received.
	   * @see Jabberlue::Message
	   * @param m The Message.
	   */
	  SigC::Signal1<void, const Jabberlue::Message&> errMessage;
	  
	  /**
	   * Message delivered event.
	   * This event is fired when a Message has been delivered.
	   * @see Jabberlue::Message
	   * @param id The ID of the Message which has been displayed.
	   * @param thread The thread of the Message which has been displayed.
	   * @param from The JabberID to which the Message which has been displayed was sent.
	   */
	  SigC::Signal3<void, const string&, const string&, const string&> msgDelivered;
	  
	  /**
	   * Message delivered event.
	   * This event is fired when a Message has been displayed.
	   * @see Jabberlue::Message
	   * @param id The ID of the Message which has been displayed.
	   * @param thread The thread of the Message which has been displayed.
	   * @param from The JabberID to which the Message which has been displayed was sent.
	   */
	  SigC::Signal3<void, const string&, const string&, const string&> msgDisplayed;
	  
	  /**
	   * Message delivered event.
	   * This event is fired when a Message has been delivered.
	   * @see Jabberlue::Message
	   * @param id The ID of the Message which has been displayed.
	   * @param thread The thread of the Message which has been displayed.
	   * @param from The JabberID to which the Message which has been displayed was sent.
	   */
	  SigC::Signal3<void, const string&, const string&, const string&> msgComposing;
	  
	  // XML transmission signals
	  /**
	   * This event is emitted when XML is transmitted.
	   * @param XML The XML which is being sent.
	   */
	  Signal1<void, const char*>        evtTransmitXML;
	  /**
	   * This event is emitted when XML is received.
	   * @param XML The XML which was received.
	   */
	  Signal1<void, const char*>        evtRecvXML;
	  
	  
	  /**
	   * Message event.
	   * This event is fired when a Message is received.
	   * @see Jabberlue::Message
	   * @param m The Message.
	   */
	  SigC::Signal1<void, const Jabberlue::Message&>    evtMessage;
	  
	  /**
	   * This event is emitted when an IQ element is received.
	   * @see jabberoo::Packet
	   * @param iq The iq element as a jabberoo::Packet.
	   */ 
	  Signal1<void, const jabberoo::Packet&>         evtIQ;
	  
	  /**
	   * This event is emitted when a Presence element is received.
	   * Clients can decide whether to display the presence update in various
	   * UI areas depending on whether the Presence::Type changed, such as
	   * unavailable to available.
	   * @see jabberoo::Presence
	   * @param p The Presence.
	   * @param changed Whether the Presence::Type changed.
	   */
	  Signal2<void, const Jabberlue::Presence&, bool>    evtPresence;
	  
	  /**
	   * This event is emitted when a Presence subscription request is received.
	   * @see jabberoo::Presence
	   * @param p The Presence subscription request.
	   */
	  Signal1<void, const Jabberlue::Presence&>    evtPresenceRequest;
	  
	  
	  // Misc signals
	  /**
	   * This event is emitted when an unknown XML element is received.
	   * @see judo::Element
	   * @param t The element's judo::Element
	   */
	  Signal1<void, const Element&>         evtUnknownPacket;
	  /**
	   * This event is emitted when an authorization error occurs.
	   * @param errorcode The error code.
	   * @param errormsg The error message.
	   */
	  Signal2<void, int, const char*>   evtAuthError;	  
	  /**
	   * This event is emitted when a client version is requested.
	   * @param name The name of the client.
	   * @param version The version of the client.
	   */
	  Signal2<void, string&, string&> evtOnVersion;
	  /**
	   * This event is emitted when idle time is requested.
	   * @param seconds The number of seconds, as a string, of idleness.
	   */
	  Signal1<void, string&>            evtOnLast;
     };
};

#endif // INCL_JABBERLUE_HH
