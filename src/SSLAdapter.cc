/* SSLAdapter.cc
 * SSL adapter for TCP connection
 *
 * Copyright (C) 1999-2001 Dave Smith & Julian Missig
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contributor(s): Konrad Podloucky
 */

#include "SSLAdapter.hh"

#include <iostream>
#include <string>
#include <openssl/err.h>

using namespace std;

SSLAdapter::SSLAdapter():
     _client_ssl(NULL), _ssl_method(NULL),
     _ssl_client_context(NULL), _connected(false)
{

     // initialize SSL library
     SSL_library_init();
     SSL_load_error_strings();

     // construct a ssl method
     _ssl_method = TLSv1_client_method();

     // construct a context
     _ssl_client_context = SSL_CTX_new(_ssl_method);

     // create SSLs
     _client_ssl = SSL_new(_ssl_client_context);
     SSL_set_ssl_method(_client_ssl, _ssl_method);

     g_assert(_client_ssl != NULL && _ssl_method != NULL && _ssl_client_context != NULL);

#ifdef TRANSMITTER_DEBUG
     cerr << "SSLAdapter constructor done." << endl;
#endif
}


SSLAdapter::~SSLAdapter() 
{
     disconnect();
     if(_client_ssl != NULL) {
	  SSL_free(_client_ssl);
     }
     if(_ssl_client_context) {
	  SSL_CTX_free(_ssl_client_context);
     }
     
     ERR_free_strings();
     ERR_remove_state(0);


#ifdef TRANSMITTER_DEBUG
     cerr << "SSLAdapter destructor done." << endl;
#endif
}


void SSLAdapter::disconnect() 
{
#ifdef TRANSMITTER_DEBUG
     cerr << "SSLAdapter disconnecting" << endl;
#endif
     
     if(_connected) {
	  SSL_shutdown(_client_ssl);
     }
}


GIOError SSLAdapter::send(const gchar* data, const guint len, guint* written) 
{
     g_assert(_connected);

     int bytes_written = SSL_write(_client_ssl, data, len);
     if(bytes_written < 0) {
	  long errorno = ERR_get_error();
	  
	  _lastError = ERR_error_string(errorno, NULL);
	  *(written) = 0;
#ifdef TRANSMITTER_DEBUG
	  cerr << "SSLAdapter::send error in write! " << _lastError << endl;
#endif
	  return(G_IO_ERROR_UNKNOWN);

     }

     g_assert(bytes_written > 0);
     *(written) = bytes_written;
#ifdef TRANSMITTER_DEBUG
     cerr << "SSLAdapter::send successful" << endl;
#endif
     return(G_IO_ERROR_NONE);
}



GIOError SSLAdapter::read(gchar* buffer, const guint count, guint* bytes_read) 
{
     cerr << "trying to read from SSL socket..." << endl;
     cerr << SSL_pending(_client_ssl) << " bytes waiting" << endl;
     *(bytes_read) = SSL_read(_client_ssl, buffer, count);

     g_assert(_connected);
     
     if(*(bytes_read) < 0) {
	  _lastError = ERR_error_string(ERR_get_error(), NULL);
	  
#ifdef TRANSMITTER_DEBUG
	  cerr << "SSLAdapter::socketRead error: " << _lastError << endl;
#endif
	  return (G_IO_ERROR_UNKNOWN);
     }
     if(*(bytes_read) == count && SSL_pending(_client_ssl) > 0) {
	  return (G_IO_ERROR_AGAIN);
     }

     return (G_IO_ERROR_NONE);
}



bool SSLAdapter::registerSocket(const gint socket)
{
#ifdef TRANSMITTER_DEBUG
     cerr << "SSLAdapter::registerConnection socket: " << socket << endl;
#endif

     if(SSL_set_fd(_client_ssl, socket) < 0) {
	  _lastError = ERR_error_string(ERR_get_error(), NULL);

#ifdef TRANSMITTER_DEBUG
	  cerr << "SSLAdapter::registerConnection SSL_set_fd failed! " << _lastError << endl;
#endif
	  return(false);
     }

     SSL_set_connect_state(_client_ssl);
     cerr <<  SSL_state_string_long(_client_ssl) << endl;

     int connect_error = SSL_connect(_client_ssl);
     cerr << connect_error << endl;
     
     if(connect_error < 0) {
	  _lastError = ERR_error_string(ERR_get_error(), NULL);
#ifdef TRANSMITTER_DEBUG
	  cerr <<  SSL_state_string_long(_client_ssl) << endl;
	  cerr << "SSLAdapter::registerConnection SSL_connect failed! " << _lastError << endl;
#endif
	  return(false);
     }

     
     if(SSL_do_handshake(_client_ssl) < 0) {
	  _lastError = ERR_error_string(ERR_get_error(), NULL);
#ifdef TRANSMITTER_DEBUG
	  cerr << "SSLAdapter::registerConnection SSL_do_handshake failed! " << _lastError << endl;
#endif
	  return(false);
     }

     // print SSL information
       SSL_CIPHER * cipher = NULL;
       X509 * cert = NULL;
       int max_bits = 0;
       int real_bits = 0;

       cipher = SSL_get_current_cipher(_client_ssl);
       real_bits = SSL_CIPHER_get_bits(cipher, &max_bits);
  
       cout << "Version: " << SSL_get_version(_client_ssl) << endl
	    << "Cipher: " << SSL_CIPHER_get_name(cipher) 
	    << " Version: " << SSL_CIPHER_get_version(cipher)
	    << " " << real_bits << "(" << max_bits << ") bits" << endl;
  
       cert = SSL_get_peer_certificate(_client_ssl);
       if(cert) {
	    EVP_PKEY * pkey = X509_get_pubkey(cert);
	    if(pkey) {
		 if(pkey->type == EVP_PKEY_RSA && pkey->pkey.rsa && pkey->pkey.rsa->n) {
		      cout << BN_num_bits(pkey->pkey.rsa->n) << " bit RSA key" << endl;
		 } else if(pkey->type == EVP_PKEY_DSA && pkey->pkey.dsa && pkey->pkey.dsa->p) {
		      cout << BN_num_bits(pkey->pkey.dsa->p) << " bit DSA key" << endl;
		 }
	    }
	    EVP_PKEY_free(pkey);
	    X509_free(cert);
       }


     _connected = true;
     return(true);
}


const string SSLAdapter::getError() 
{
  return (_lastError);
}

