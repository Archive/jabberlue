/* jabberlue-conf-read.hh
 * Centralized configuration of Jabber Stuff
 *
 * Copyright (c) 2001 IBM Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contributor(s): Julian Missig
 */

#ifndef INCL_JABBERLUE_CONF_WRITE_HH
#define INCL_JABBERLUE_CONF_WRITE_HH

//#include <sigc++/object.h>
//#include <sigc++/signal_system.h>
#include <gconf/gconf-client.h>
#include <string>

// All of the paths
//const std::string JABBERCONFDIR = "/apps/jabber";
//const std::string CONNECTIONDIR = "/apps/jabber/connection";
//const std::string USERINFODIR   = "/apps/jabber/userinfo";
//const std::string GPGDIR        = "/apps/jabber/gpg";

class JabberlueConfWrite
//     : public SigC::Object
{
public:
     JabberlueConfWrite(int argc, gchar *argv[]);
     ~JabberlueConfWrite();
     void sync();
protected:
     std::string getString(gchar* gtemp);
public:
     // GConfClient
     GConfClient* getGConfClient() { return _gc; }
     // Connection
     void set_username(const std::string& username);
     void set_save_password(bool save_password);
     void set_password(const std::string& password);
     void set_server(const std::string& server);
     void set_port(gint port);
     void set_ssl(bool ssl);
     
     // User Info
     void set_given_name(const std::string& given_name);
     void set_family_name(const std::string& family_name);
     void set_full_name(const std::string& full_name);
     void set_nickname(const std::string& nickname);
     void set_email(const std::string& email);
     void set_country(const std::string& country);

private:
     GConfClient*     _gc;

public:
//     SigC::Signal0<void> connection_conf_changed;
//     SigC::Signal0<void> userinfo_conf_changed;
};

#endif // INCL_JABBERLUE_CONF_WRITE_HH
